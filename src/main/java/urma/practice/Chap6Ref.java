package urma.practice;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Adam on 7/22/2015.
 */

import static java.util.stream.Collectors.*;

public class Chap6Ref {


    /*

       new Movie("pixels", false, 91, Movie.Rating.G),
       new Movie("10 cent pistol", false, 82, Movie.Rating.R),
       new Movie("paper towns", false, 99, Movie.Rating.R),
       new Movie("ant man", true, 5, Rating.PG13),
       new Movie("trainwreck", true, 62, Movie.Rating.PG13),
       new Movie("inside out", true, 81, Movie.Rating.G),
       new Movie("the gallows", true, 32, Movie.Rating.PG),
       new Movie("minious", false, 56, Movie.Rating.PG),
       new Movie("jurasic world", false, 45, Movie.Rating.R));
     */


    public static void main(String[] args) {

        //Count playing Rated-R movies:
       long count =  Movie.movies.stream()
                .filter(Movie::isPlaying)
                .filter(m -> m.getRating() == Movie.Rating.R)
                .count();
        System.out.println("Count playing Rated-R movies are" + count);

       //Print out Rated-G movies that are playing:
        Movie.movies.parallelStream()
                .filter(m -> m.getRating() == Movie.Rating.R)
                .filter(m -> m.isPlaying() == false)
                .map(m -> m.getName())
                .forEach(System.out::println);


        //System.out.println("Movies grouped by Rating: ");/

        Map<Movie.Rating, List<Movie>> moviesByType =
                Movie.movies.stream().collect(groupingBy(Movie::getRating));

        System.out.println(moviesByType);

        //movies grouped by rating and thjen by playiing
        Map<Movie.Rating, Map<Boolean, List<Movie>>> moviesByTypeandPlaying =
                Movie.movies.stream().collect(groupingBy(Movie::getRating,
                        groupingBy(m -> m.isPlaying())
                        ));

        System.out.println(moviesByTypeandPlaying);




    }

}
